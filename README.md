
```ts
interface ITransaction {
    id: number
    user: string
    currenctBalance: number
    transactionAmount: number
}

const testValues: ITransaction[] = [
    {
        id: 1,
        user: "test_1",
        currenctBalance: 2000,
        transactionAmount: 4000
    },
    {
        id: 2,
        user: "test_2",
        currenctBalance: 2000,
        transactionAmount: 4001
    }

]

function amountOfBedtByUser(transactions: ITransaction[], minDebtValue: number = 2000): Map<string, number>{
    const usersDebt = new Map()

    transactions.forEach(transaction => {
        const balanceAfterTransaction = transaction.currenctBalance - transaction.transactionAmount

        if(balanceAfterTransaction <= -minDebtValue){
            usersDebt.set(transaction.user, Math.abs(balanceAfterTransaction))
        } else
        {
            usersDebt.delete(transaction.user)
        }             
    })

    return usersDebt
}

function totalAmountOfBedt(transactions: ITransaction[], minDebtValue: number = 2000): number{
    const usersDebt = new Map()
    let result = 0

    for (let index = transactions.length - 1; index >= 0; index--) {
        const balanceAfterTransaction = transactions[index].currenctBalance - transactions[index].transactionAmount
        if(balanceAfterTransaction <= -minDebtValue && !usersDebt.get(transactions[index].user)){
            usersDebt.set(transactions[index].user, balanceAfterTransaction)             
            result += balanceAfterTransaction
        }
    }
 
    return Math.abs(result)
}


const amountByUser = amountOfBedtByUser(testValues)
const totalAmount = totalAmountOfBedt(testValues) 
console.log(amountByUser, 'Debt amount by user'),
console.log(totalAmount, 'Total bedt amount')
```



<details><summary><b>Output</b></summary>

```ts
"use strict";
const testValues = [
    {
        id: 1,
        user: "test_1",
        currenctBalance: 2000,
        transactionAmount: 4000
    },
    {
        id: 2,
        user: "test_2",
        currenctBalance: 2000,
        transactionAmount: 4001
    }
];
function amountOfBedtByUser(transactions, minDebtValue = 2000) {
    const usersDebt = new Map();
    transactions.forEach(transaction => {
        const balanceAfterTransaction = transaction.currenctBalance - transaction.transactionAmount;
        if (balanceAfterTransaction <= -minDebtValue) {
            usersDebt.set(transaction.user, Math.abs(balanceAfterTransaction));
        }
        else {
            usersDebt.delete(transaction.user);
        }
    });
    return usersDebt;
}
function totalAmountOfBedt(transactions, minDebtValue = 2000) {
    const usersDebt = new Map();
    let result = 0;
    for (let index = transactions.length - 1; index >= 0; index--) {
        const balanceAfterTransaction = transactions[index].currenctBalance - transactions[index].transactionAmount;
        if (balanceAfterTransaction <= -minDebtValue && !usersDebt.get(transactions[index].user)) {
            usersDebt.set(transactions[index].user, balanceAfterTransaction);
            result += balanceAfterTransaction;
        }
    }
    return Math.abs(result);
}
const amountByUser = amountOfBedtByUser(testValues);
const totalAmount = totalAmountOfBedt(testValues);
console.log(amountByUser, 'Debt amount by user'),
    console.log(totalAmount, 'Total bedt amount');

```


</details>


<details><summary><b>Compiler Options</b></summary>

```json
{
  "compilerOptions": {
    "strict": true,
    "noImplicitAny": true,
    "strictNullChecks": true,
    "strictFunctionTypes": true,
    "strictPropertyInitialization": true,
    "strictBindCallApply": true,
    "noImplicitThis": true,
    "noImplicitReturns": true,
    "alwaysStrict": true,
    "esModuleInterop": true,
    "declaration": true,
    "target": "ES2017",
    "jsx": "react",
    "module": "ESNext",
    "moduleResolution": "node"
  }
}
```


</details>

**Playground Link:** [Provided](https://www.typescriptlang.org/play/?#code/JYOwLgpgTgZghgYwgAgJIBUpxAZ0WYAexGQG8AoZK5YAEwC5kQBXAWwCNpLrmdpGcYKKADm3KgmZQoEEAjAAhOABtsSRiw5dqyIdjzyiIAIKtCzcBraco5AL7lyCYoN0RBANRXN3jDFlx8IwBtAF1kAF5kYPEyWJ06RgBGABp4nj4oRgAiSEEAfSTstJ0dSWlZeSVVOQhGACYABmaS0qo9QMNiU3NLZAAWZsbYu1aqCjaqROR6sdLefmRc9zB8+uL0iSkZOUUVNTqZobmdDoMCbrMLMEZBxqSRx1DHGAsukjgr8AB5GAUIWiKACeAFVMgAKM5BFx+TD6aEgMIpZCsUAAEQg7DAXmUPisWigkSOzQAlIwALJwAAOAB5BMIQCJkZobAA+CbUZy4MDIBZQHAYrFEkAQADuyEpVPBJMcpwC5yMOAAdDBCFAAKKIAAWkPlCMirLik2QXNc7H2tWMMEgUDhnQuJCiUPeSvKOyqFqQyAAtLo9S7nQ6etdZZNgDBweaakgrTa7QriMgaVFvaiQILsd4ICSOcaqHyBZiwEq+GBdfCXXzkZSwFqlXB2DhI56ILHoPGESSZca7MgIMo+JsjXn85lC1ilbR+xBIOX7UYlXzu5NeyOqCMZbEZGApCQCxn7C83g7dIQwCpgz8-gCy4HFbD-Q6kSj0UWcXimNZoESmqT8TZcxNFweX3IthTFCVqWlWJlBnZAZBwZhlB5KJhliVVCXBWCeVAKcAA8iTvFwlVgxlax9ZAkgAbhoEB8OQVlUJo3CIDw71vRJYc2lNHkowONtbUfIxCKElxghYvDQldbZKj2aMUF9IjcHEujWKkpTLzAIdw2beSBI7d4kxTNMM3fFAADJzOQABCUCJxEGc5wTZSJKkpccyHHQ7OLUsnIRHAVPwtzMmRPjLWtdtRJATi12QTzqAQpCeQAaiiMKYwiwSKwdIcHB0PKtxnXdINretG3BRLkJlBxHB45BPl6YEwW-KIGuuX5-kBBRQQhPJM1xdwZTqsAzwvL4UNPc9lE0jqb0hFYzJwTinBcQhYJIwgRHBNrwG65qoGRAByDN6vG5B2CBXlMkOkk0lNNaIA2raRqmzSjvQUblHOm9Tsam6gA)
      